#!/bin/bash

VM_NAME=dev

echo "======================================================================================================="
echo "=== build jar file and docker image"
echo "======================================================================================================="

mvn clean package docker:build -PunzipFatJar -PbuildDockerImageWithSpotifyAndPushToGiantSwarm -DpushImage=true -DskipTests=true
