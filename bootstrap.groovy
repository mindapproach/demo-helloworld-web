
// define parameter consts
def MAVEN_PROFILE = "buildDockerImageWithSpotifyAndPushToPrivateRegistry"

// ---

def flow
node{
  echo "bootstrap function begin"
  echo "pwd: " + pwd()
  git url: 'https://bitbucket.org/mindapproach/demo-helloworld-web.git'
  flow = load "flow.groovy"
  echo "bootstrap function end"
}
flow.internalBuild( MAVEN_PROFILE )
