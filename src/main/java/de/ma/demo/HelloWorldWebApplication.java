package de.ma.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldWebApplication 
{
  public static void main(String[] args) throws InterruptedException 
  {
  	Thread.sleep( 5000L );
    SpringApplication.run( HelloWorldWebApplication.class, args );
  }
}