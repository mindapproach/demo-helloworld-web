package de.ma.demo;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = HelloWorldWebApplication.class )
@WebAppConfiguration
@IntegrationTest( { "server.port=0" } )
@ActiveProfiles( "dev" )
public class HelloWorldIntegrationTest
{
	@Value( "${local.server.port}" )
	private int port;
	private URL base;
	
	private RestTemplate template;
	
	@Before
	public void setUp() throws Exception
	{
		this.base     = new URL( "http://localhost:" + this.port + "/" );
		this.template = new RestTemplate();
	}
	
	@Test
	public void getHelloWorld() throws Exception
	{
		ResponseEntity<String> response = this.template.getForEntity( this.base.toString(), String.class );
		assertThat( response.getBody(), equalTo( "Hello World!" ) );
	}
}
